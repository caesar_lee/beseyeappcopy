//
//  SceneDelegate.h
//  BeseyeAppCopy
//
//  Created by caesar on 2020/3/7.
//  Copyright © 2020 WenHan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

